package com.advisoryme.ibest.bus.data.network

import okhttp3.Interceptor

interface ApiManager {
    fun <T> createApi(apiClass: Class<T>): T

    fun buildClient(vararg interceptors: Interceptor)
}