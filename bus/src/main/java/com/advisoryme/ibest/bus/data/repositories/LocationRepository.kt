package com.advisoryme.ibest.bus.data.repositories

import android.app.AlarmManager
import android.app.PendingIntent
import android.arch.lifecycle.LiveData
import android.content.Context
import android.content.Context.ALARM_SERVICE
import android.content.Intent
import com.advisoryme.ibest.bus.data.models.IBestLocation
import com.advisoryme.ibest.bus.data.models.RepositoryResult
import com.advisoryme.ibest.bus.data.models.events.LocationServiceStatusChangedEvent
import com.advisoryme.ibest.bus.data.network.location.LocationReceiver
import com.advisoryme.ibest.bus.utils.Const
import com.advisoryme.ibest.bus.utils.InjectionUtils
import org.greenrobot.eventbus.EventBus

class LocationRepository(private val context: Context) : BaseRepository() {
    private val locationApi = InjectionUtils.provideLocationApi()
    private val prefsManager = InjectionUtils.providePrefsManager(context)
    private val alarmManager = context.getSystemService(ALARM_SERVICE) as AlarmManager

    fun startLocationUpdates() {
        // get the pending intent
        val pendingIntent = getLocationReceiverIntent()

        // schedule the task using alarm manager
        val currentTime = System.currentTimeMillis()
        alarmManager.setRepeating(
                AlarmManager.RTC_WAKEUP,
                currentTime + LOCATION_UPDATES_PERIOD,
                LOCATION_UPDATES_PERIOD,
                pendingIntent)

        // update the service status
        updateLocationServiceStatus(true)
    }

    fun stopLocationUpdates() {
        val pendingIntent = getLocationReceiverIntent()
        pendingIntent.cancel()
        alarmManager.cancel(pendingIntent)

        // update the service status
        updateLocationServiceStatus(false)
    }

    private fun getLocationReceiverIntent(): PendingIntent {
        val intent = Intent(context.applicationContext, LocationReceiver::class.java)
        return PendingIntent.getBroadcast(context.applicationContext, Const.ALARM_REQ_LOCATION_UPDATES, intent, PendingIntent.FLAG_UPDATE_CURRENT)
    }

    fun updateLocation(lat: Double, lng: Double, speed: Int, distance: Float, timestamp: Long):
            LiveData<RepositoryResult<Boolean>> {
        val call = locationApi.updateLocation(lat, lng, speed, distance, timestamp)
        return handleApiResponse(call)
    }

    fun getLastLocation() = prefsManager.load<IBestLocation>(Const.KEY_LAST_LOCATION, IBestLocation::class.java)

    fun updateLastLocation(location: IBestLocation) = prefsManager.save(location, Const.KEY_LAST_LOCATION)

    private fun updateLocationServiceStatus(running: Boolean) {
        prefsManager.save(running, Const.KEY_LOCATION_SERVICE_STATUS)
        EventBus.getDefault().post(LocationServiceStatusChangedEvent(running))
    }

    fun getLocationServiceStatus() = prefsManager.load<Boolean>(Const.KEY_LOCATION_SERVICE_STATUS, Boolean::class.java)

    companion object {
        private const val LOCATION_UPDATES_PERIOD = 60 * 1000L // 1 minute
    }
}
