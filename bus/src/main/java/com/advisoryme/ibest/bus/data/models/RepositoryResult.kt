package com.advisoryme.ibest.bus.data.models

data class RepositoryResult<T>(val result: T?,
                               val message: String?
)