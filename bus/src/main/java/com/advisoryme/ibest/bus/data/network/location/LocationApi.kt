package com.advisoryme.ibest.bus.data.network.location

import com.advisoryme.ibest.bus.data.network.bodies.UpdateLocationBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface LocationApi {
    @POST("location")
    fun updateLocation(@Body body: UpdateLocationBody): Call<Void>
}