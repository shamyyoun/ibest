package com.advisoryme.ibest.bus.data.models

data class IBestLocation(val lat: Double = 0.0,
                         val lng: Double = 0.0,
                         val timestamp: Long = 0)