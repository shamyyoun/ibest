package com.advisoryme.ibest.bus.data.repositories

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.advisoryme.ibest.bus.data.models.RepositoryResult
import com.advisoryme.ibest.bus.utils.InjectionUtils
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

open class BaseRepository {

    protected inline fun <reified CallResult, reified Result> handleApiResponse(call: Call<CallResult>, noinline onSuccess: ((liveData: LiveData<RepositoryResult<Result>>) -> Any)? = null): LiveData<RepositoryResult<Result>> {
        val liveData = MutableLiveData<RepositoryResult<Result>>()

        call.enqueue(object : Callback<CallResult> {
            override fun onResponse(call: Call<CallResult>, response: Response<CallResult>) {
                when (response.isSuccessful) {
                    true -> {
                        // check Result class type to get suitable result
                        val result = when (Result::class) {
                            Boolean::class -> true as Result
                            CallResult::class -> response.body() as Result
                            else -> null
                        }

                        // check if result is not null trigger the liveData
                        if (result != null) {
                            liveData.value = InjectionUtils.provideRepositoryResult(result = result)
                        }

                        // invoke the onSuccess callback
                        onSuccess?.invoke(liveData)
                    }
                    else -> {
                        // get the message
                        val message: String? = try {
                            JSONObject(response.errorBody()?.string()).getString("message")
                        } catch (e: Exception) {
                            null
                        }

                        liveData.value = InjectionUtils.provideRepositoryResult(message = message)
                    }
                }
            }

            override fun onFailure(call: Call<CallResult>, t: Throwable) {
                liveData.value = null
            }
        })

        return liveData
    }
}