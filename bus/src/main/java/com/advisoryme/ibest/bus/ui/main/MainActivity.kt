package com.advisoryme.ibest.bus.ui.main

import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.advisoryme.ibest.bus.R
import com.advisoryme.ibest.bus.ui.base.BaseActivity
import com.advisoryme.ibest.bus.utils.InjectionUtils
import com.advisoryme.ibest.bus.utils.LocationUtils
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity<MainViewModel>() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun getTheViewModel() = InjectionUtils.provideMainViewModel(this)

    override fun observe() {
        super.observe()

        viewModel.locationEvent.observe(this, Observer {
            updateUI(it == true)
        })
    }

    private fun updateUI(serviceRunning: Boolean) {
        when (serviceRunning) {
            true -> {
                tvStatus.setText(R.string.service_is_running)
                tvStatus.setBackgroundResource(R.color.raw_green)
            }
            else -> {
                tvStatus.setText(R.string.service_is_not_running_tap_to_start)
                tvStatus.setBackgroundResource(R.color.red)
            }
        }
    }

    fun changeLocationStatus(v: View) {
        when (viewModel.locationEvent.value) {
            true -> stopLocationUpdates()
            else -> startLocationUpdates()
        }
    }

    private fun startLocationUpdates() {
        // check validations then start the location updates if they're ok
        if (checkLocationUpdatesServicesValidations()) {
            viewModel.startLocationUpdates()
        }
    }

    private fun stopLocationUpdates() = viewModel.stopLocationUpdates()

    private fun checkLocationUpdatesServicesValidations(): Boolean {
        // check permissions
        if (!LocationUtils.checkLocationPermissions(this)) {
            // show error
            showLongToast(R.string.location_permissions_are_not_granted_grant_them)

            // request permissions
            LocationUtils.requuireLocationPermissions(this)

            return false
        }

        // check gps
        if (!LocationUtils.checkGps(this)) {
            // show error
            showLongToast(R.string.gps_is_not_enabled_enable)

            // open settings activity
            LocationUtils.openLocationSettingsActivity(this)

            return false
        }

        return true
    }

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, MainActivity::class.java)
        }
    }
}
