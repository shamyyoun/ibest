package com.advisoryme.ibest.bus.utils

object Const {
    // app constants
    const val END_POINT = "http://142.93.46.191/api/"
    const val PREFS_NAME = "ibest_bus"
    const val LOG_TAG = "iBest Bus"
    const val APP_FILES_DIR = "/.ibest_bus"

    // keys
    const val KEY_LAST_LOCATION = "last_location"
    const val KEY_LOCATION_SERVICE_STATUS = "location_service_status"

    // notifications
    const val NOTI_PERMISSION_NOT_GRANTED = 1
    const val NOTI_GPS_IS_NOT_ENABLED = 2

    // alarm requests
    const val ALARM_REQ_LOCATION_UPDATES = 1
}