package com.advisoryme.ibest.parents.data.models

data class User(
        val children: List<Child>? = ArrayList(),
        val token: String = "")