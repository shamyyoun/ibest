package com.advisoryme.ibest.parents.data.apis.notifications

import com.advisoryme.ibest.parents.data.apis.interceptors.AuthorizationInterceptor
import com.advisoryme.ibest.parents.data.apis.utils.ApiInjectionUtils
import com.advisoryme.ibest.parents.utils.InjectionUtils
import retrofit2.Call

class NotificationsApiImpl(token: String) {
    private val notificationsApi: NotificationsApi

    init {
        val authInterceptor = AuthorizationInterceptor(token)
        val apiManagerImpl = InjectionUtils.provideApiManager(authInterceptor)
        notificationsApi = apiManagerImpl.createApi(NotificationsApi::class.java)
    }

    fun updateFCMToken(fcmToken: String): Call<Void> {
        val body = ApiInjectionUtils.provideUpdateFCMTokenBody(fcmToken)
        return notificationsApi.updateFCMToken(body)
    }
}