package com.advisoryme.ibest.parents.data.apis.models

import com.advisoryme.ibest.parents.data.models.Location

data class LocationsResponse(val locations: List<Location> = ArrayList())