package com.advisoryme.ibest.parents.ui.signup

import android.arch.lifecycle.Observer
import android.support.v7.app.AppCompatActivity
import android.view.inputmethod.EditorInfo
import com.advisoryme.ibest.parents.R
import com.advisoryme.ibest.parents.data.models.User
import com.advisoryme.ibest.parents.ui.base.BaseDialog
import com.advisoryme.ibest.parents.utils.InjectionUtils
import kotlinx.android.synthetic.main.dialog_add_password.*

/**
 * Created by Shamyyoun on 2/17/2016.
 */
class AddPasswordDialog(activity: AppCompatActivity, private val user: User, private val passwordChangeCallback: () -> Unit) : BaseDialog<AddPasswordViewModel>(activity) {
    init {
        setContentView(R.layout.dialog_add_password)
        setTitle(R.string.add_password)
    }

    override fun setContentView(layoutResId: Int) {
        super.setContentView(layoutResId)

        // add listeners
        btnConfirm.setOnClickListener { addPassword() }
        etPassword.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                addPassword()
            }
            true
        }
    }

    override fun getTheViewModel() = InjectionUtils.provideAddPasswordViewModel(activity, user)

    override fun observe() {
        super.observe()

        // add password event
        viewModel.addPasswordEvent.observe(activity, Observer {
            if (it == true) {
                // show msg and invoke the callback
                showLongToast(R.string.added_successfully)
                passwordChangeCallback.invoke()
            }
        })
    }

    private fun addPassword() = viewModel.addPassword(etPassword.text.toString())
}
