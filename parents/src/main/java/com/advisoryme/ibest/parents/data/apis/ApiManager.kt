package com.advisoryme.ibest.parents.data.apis

import okhttp3.Interceptor

interface ApiManager {
    fun <T> createApi(apiClass: Class<T>): T

    fun buildClient(vararg interceptors: Interceptor)
}