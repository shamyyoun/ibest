package com.advisoryme.ibest.parents.utils

object Const {
    // app constants
    const val END_POINT = "http://142.93.46.191/api/"
    const val PREFS_NAME = "ibest_parents"
    const val LOG_TAG = "iBest Parents"
    const val APP_FILES_DIR = "/.ibest_parents"

    // keys
    const val KEY_ACTIVE_USER = "active_user"
    const val KEY_FCM_TOKEN_UPDATED = "fcm_token_updated"
}