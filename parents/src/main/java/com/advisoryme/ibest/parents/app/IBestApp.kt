package com.advisoryme.ibest.parents.app

import android.app.Application
import com.advisoryme.ibest.parents.R
import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric
import uk.co.chrisjenx.calligraphy.CalligraphyConfig


class IBestApp : Application() {
    override fun onCreate() {
        super.onCreate()

        // set custom font
        CalligraphyConfig.initDefault(CalligraphyConfig.Builder()
                .setDefaultFontPath("font.otf")
                .setFontAttrId(R.attr.fontPath)
                .build())

        // init fabric
        Fabric.with(this, Crashlytics())
    }
}