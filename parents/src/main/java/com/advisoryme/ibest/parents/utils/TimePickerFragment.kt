package com.advisoryme.ibest.parents.utils

import android.app.Dialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.Window
import com.advisoryme.ibest.parents.R
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale

/**
 * Created by Shamyyoun on 9/11/2015.
 */
class TimePickerFragment : DialogFragment() {
    var timePickerListener: TimePickerDialog.OnTimeSetListener? = null
    private var calendar: Calendar? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        if (calendar == null) {
            // Use the current date as the default date in the picker
            calendar = Calendar.getInstance()
        }

        val hour = calendar!!.get(Calendar.HOUR_OF_DAY)
        val minute = calendar!!.get(Calendar.MINUTE)

        // create TimePickerDialog instance and customize it
        val timePickerDialog = TimePickerDialog(activity, R.style.MyDialog,
                timePickerListener, hour, minute, false)

        timePickerDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

        return timePickerDialog
    }

    fun setTime(calendar: Calendar) {
        this.calendar = calendar
    }

    fun setTime(time: String?, timeFormat: String) {
        if (time != null) {
            try {
                this.calendar = Calendar.getInstance()
                val sdf = SimpleDateFormat(timeFormat, Locale.getDefault())
                this.calendar!!.time = sdf.parse(time)
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }
}