package com.advisoryme.ibest.parents.ui.main

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import android.os.Handler
import com.advisoryme.ibest.parents.R
import com.advisoryme.ibest.parents.data.models.Location
import com.advisoryme.ibest.parents.ui.base.BaseViewModel
import com.advisoryme.ibest.parents.utils.InjectionUtils
import com.google.firebase.iid.FirebaseInstanceId


class MainViewModel(app: Application) : BaseViewModel(app) {
    private val activeUserRepository = InjectionUtils.provideActiveUserRepository(app)
    private val locationsRepository = InjectionUtils.provideLocationsRepository(activeUserRepository.getUser()?.token!!)
    private val notificationsRepository = InjectionUtils.provideNotificationsRepository(app, activeUserRepository.getUser()?.token!!)
    val locationsEvent = MutableLiveData<List<Location>>()
    val logoutEvent = MutableLiveData<Boolean>()
    private val locationsHandler = Handler()
    private val locationsRunnable = Runnable { fetchLocations() }

    fun startFetchingLocations() = locationsHandler.post(locationsRunnable)

    private fun fetchLocations() {
        locationsRepository.locations().observeForever { locations ->
            if (locations != null) {
                for (location in locations) {
                    val child = activeUserRepository.getUser()?.children?.filter { location.id == it.id }
                    if (child != null && child.isNotEmpty()) {
                        location.icon = child[0].photo
                    }
                }
                locationsEvent.value = locations
            } else {
                // should logout
                triggerError(R.string.your_session_is_expired)
                logout()
            }

            locationsHandler.postDelayed(locationsRunnable, LOCATION_UPDATES_DELAY)
        }
    }

    fun logout() {
        activeUserRepository.removeUserData()
        logoutEvent.value = true
    }

    fun stopFetchingLocations() = locationsHandler.removeCallbacks(locationsRunnable)

    fun updateFCMToken() {
        // check if already updated
        if (notificationsRepository.isFCMTokenUpdated() != true) {
            // get the fcm token and update it
            FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener { repoResult ->
                if (repoResult.isSuccessful && repoResult.result?.token != null) {
                    notificationsRepository.updateFCMToken(repoResult.result!!.token).observeForever {
                        if (it?.result == true) {
                            notificationsRepository.setFCMTokenUpdated()
                        }
                    }
                }
            }
        }
    }

    companion object {
        private const val LOCATION_UPDATES_DELAY = 6000L
    }
}