package com.advisoryme.ibest.parents.utils

import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.support.v7.app.AppCompatActivity
import com.advisoryme.ibest.parents.data.apis.ApiManagerImpl
import com.advisoryme.ibest.parents.data.apis.authentication.AuthenticationApiImpl
import com.advisoryme.ibest.parents.data.apis.locations.LocationsApiImpl
import com.advisoryme.ibest.parents.data.apis.notifications.NotificationsApiImpl
import com.advisoryme.ibest.parents.data.apis.updatepassword.UpdatePasswordApiImpl
import com.advisoryme.ibest.parents.data.database.manager.DatabaseManager
import com.advisoryme.ibest.parents.data.models.Notification
import com.advisoryme.ibest.parents.data.models.RepositoryResult
import com.advisoryme.ibest.parents.data.models.User
import com.advisoryme.ibest.parents.data.prefs.PrefsManagerImpl
import com.advisoryme.ibest.parents.data.repositories.*
import com.advisoryme.ibest.parents.ui.login.LoginViewModel
import com.advisoryme.ibest.parents.ui.main.ChangePasswordViewModel
import com.advisoryme.ibest.parents.ui.main.MainViewModel
import com.advisoryme.ibest.parents.ui.notifications.NotificationsViewModel
import com.advisoryme.ibest.parents.ui.shared.ViewModelCustomProvider
import com.advisoryme.ibest.parents.ui.signup.AddPasswordViewModel
import com.advisoryme.ibest.parents.ui.signup.SignupViewModel
import com.advisoryme.ibest.parents.ui.splash.SplashViewModel
import okhttp3.Interceptor
import java.util.*

object InjectionUtils {
    fun providePrefsManager(context: Context) = PrefsManagerImpl(context)

    fun provideApiManager(vararg interceptors: Interceptor) = ApiManagerImpl.instance(*interceptors)

    fun provideAuthenticationRepository() = AuthenticationRepository()

    fun provideAuthenticationApi() = AuthenticationApiImpl()

    fun provideActiveUserRepository(context: Context) = ActiveUserRepository.instance(context)

    fun provideSplashViewModel(activity: AppCompatActivity) =
            ViewModelProviders.of(
                    activity,
                    ViewModelCustomProvider(SplashViewModel(activity.application)))
                    .get(SplashViewModel::class.java)

    fun provideNotificationsRepository(context: Context, token: String) = NotificationsRepository(context, token)

    fun provideNotificationsViewModel(activity: AppCompatActivity) =
            ViewModelProviders.of(
                    activity,
                    ViewModelCustomProvider(NotificationsViewModel(activity.application)))
                    .get(NotificationsViewModel::class.java)

    fun <T> provideRepositoryResult(result: T? = null, message: String? = null) = RepositoryResult(result, message)

    fun provideSignupViewModel(activity: AppCompatActivity) =
            ViewModelProviders.of(
                    activity,
                    ViewModelCustomProvider(SignupViewModel(activity.application)))
                    .get(SignupViewModel::class.java)

    fun provideLoginViewModel(activity: AppCompatActivity) =
            ViewModelProviders.of(
                    activity,
                    ViewModelCustomProvider(LoginViewModel(activity.application)))
                    .get(LoginViewModel::class.java)

    fun provideMainViewModel(activity: AppCompatActivity) =
            ViewModelProviders.of(
                    activity,
                    ViewModelCustomProvider(MainViewModel(activity.application)))
                    .get(MainViewModel::class.java)

    fun provideAddPasswordViewModel(activity: AppCompatActivity, user: User) =
            ViewModelProviders.of(
                    activity,
                    ViewModelCustomProvider(AddPasswordViewModel(activity.application, user)))
                    .get(AddPasswordViewModel::class.java)

    fun provideChangePasswordViewModel(activity: AppCompatActivity) =
            ViewModelProviders.of(
                    activity,
                    ViewModelCustomProvider(ChangePasswordViewModel(activity.application)))
                    .get(ChangePasswordViewModel::class.java)

    fun provideUpdatePasswordApi(token: String) = UpdatePasswordApiImpl(token)

    fun provideUpdatePasswordRepository(token: String) = UpdatePasswordRepository(token)

    fun provideLocationsApi(token: String) = LocationsApiImpl(token)

    fun provideLocationsRepository(token: String) = LocationsRepository(token)

    fun provideNotificationsApi(token: String) = NotificationsApiImpl(token)

    fun provideNotificationDao(context: Context) = DatabaseManager.getDatabase(context).notificationDao()

    fun proivdeNotification(content: String) = Notification(content = content, date = Calendar.getInstance().timeInMillis)
}