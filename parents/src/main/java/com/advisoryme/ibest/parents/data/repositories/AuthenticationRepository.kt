package com.advisoryme.ibest.parents.data.repositories

import android.arch.lifecycle.LiveData
import com.advisoryme.ibest.parents.data.models.RepositoryResult
import com.advisoryme.ibest.parents.data.models.User
import com.advisoryme.ibest.parents.utils.InjectionUtils

class AuthenticationRepository() : BaseRepository() {
    private val authenticationApi = InjectionUtils.provideAuthenticationApi()

    fun signup(phoneNumber: String): LiveData<RepositoryResult<Boolean>> {
        val call = authenticationApi.signup(phoneNumber)
        return handleApiResponse(call)
    }

    fun confirmCode(phoneNumber: String, code: Int): LiveData<RepositoryResult<User>> {
        val call = authenticationApi.confirmCode(phoneNumber, code)
        return handleApiResponse(call)
    }

    fun login(phoneNumber: String, password: String): LiveData<RepositoryResult<User>> {
        val call = authenticationApi.login(phoneNumber, password)
        return handleApiResponse(call)
    }
}
