package com.advisoryme.ibest.parents.ui.main

import android.arch.lifecycle.Observer
import android.support.v7.app.AppCompatActivity
import android.view.inputmethod.EditorInfo
import com.advisoryme.ibest.parents.R
import com.advisoryme.ibest.parents.ui.base.BaseDialog
import com.advisoryme.ibest.parents.utils.InjectionUtils
import kotlinx.android.synthetic.main.dialog_change_password.*

/**
 * Created by Shamyyoun on 2/17/2016.
 */
class ChangePasswordDialog(activity: AppCompatActivity) : BaseDialog<ChangePasswordViewModel>(activity) {
    init {
        setContentView(R.layout.dialog_change_password)
        setTitle(R.string.change_password)
    }

    override fun setContentView(layoutResId: Int) {
        super.setContentView(layoutResId)

        // add listeners
        btnConfirm.setOnClickListener { changePassword() }
        etRePassword.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                changePassword()
            }
            true
        }
    }

    override fun getTheViewModel() = InjectionUtils.provideChangePasswordViewModel(activity)

    override fun observe() {
        super.observe()

        // change password event
        viewModel.changePasswordEvent.observe(activity, Observer {
            if (it == true) {
                // show msg and dismiss the dialog
                showLongToast(R.string.changed_successfully)
                dismiss()
            }
        })
    }

    private fun changePassword() = viewModel.changePassword(etOldPassword.text.toString(), etNewPassword.text.toString(), etRePassword.text.toString())
}
