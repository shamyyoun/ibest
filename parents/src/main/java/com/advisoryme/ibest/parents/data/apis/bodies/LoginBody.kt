package com.advisoryme.ibest.parents.data.apis.bodies

import com.google.gson.annotations.SerializedName

data class LoginBody(
        @SerializedName("phone_number")
        val phoneNumber: String = "",
        @SerializedName("password")
        val password: String = ""
)
