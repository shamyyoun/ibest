package com.advisoryme.ibest.parents.data.apis.authentication

import com.advisoryme.ibest.parents.data.apis.bodies.ConfirmCodeBody
import com.advisoryme.ibest.parents.data.apis.bodies.LoginBody
import com.advisoryme.ibest.parents.data.apis.bodies.SignupBody
import com.advisoryme.ibest.parents.data.models.User
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface AuthenticationApi {
    @POST("signup")
    fun signup(@Body body: SignupBody): Call<Void>

    @POST("confirm_code")
    fun confirmCode(@Body body: ConfirmCodeBody): Call<User>

    @POST("signin")
    fun login(@Body login: LoginBody): Call<User>
}