package com.advisoryme.ibest.parents.data.database.manager

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.advisoryme.ibest.parents.data.database.daos.NotificationDao
import com.advisoryme.ibest.parents.data.models.Notification

@Database(entities = [Notification::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun notificationDao(): NotificationDao
}