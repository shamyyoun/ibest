package com.advisoryme.ibest.parents.data.models

import com.google.gson.annotations.SerializedName

data class Location(val id: Int = 0,
                    val lat: Double = 0.0,
                    @SerializedName("long")
                    val lng: Double = 0.0,
                    var icon: String = "")