package com.advisoryme.ibest.parents.ui.notifications

import android.view.View
import android.widget.TextView
import com.advisoryme.ibest.parents.R
import com.advisoryme.ibest.parents.data.models.Notification
import com.advisoryme.ibest.parents.ui.base.BaseRecyclerViewHolder
import com.advisoryme.ibest.parents.utils.DateUtils
import org.ocpsoft.prettytime.PrettyTime

class NotificationViewHolder(rootView: View) : BaseRecyclerViewHolder(rootView) {
    private lateinit var item: Notification
    private val tvDate = findViewById(R.id.tvDate) as TextView
    private val tvContent = findViewById(R.id.tvContent) as TextView

    fun bind(item: Notification) {
        with(item) {
            this@NotificationViewHolder.item = this
            tvContent.text = content
            val date = PrettyTime().format(DateUtils.convertToCalendar(date))
            tvDate.text = date
        }
    }
}