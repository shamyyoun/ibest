package com.advisoryme.ibest.parents.ui.splash

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import com.advisoryme.ibest.parents.ui.base.BaseViewModel
import com.advisoryme.ibest.parents.utils.InjectionUtils

class SplashViewModel(app: Application) : BaseViewModel(app) {
    private val activeUserRepository = InjectionUtils.provideActiveUserRepository(app)
    val hasUserEvent = MutableLiveData<Boolean>()

    fun startSplash() {
        // trigger has user event
        hasUserEvent.value = activeUserRepository.hasActiveUser()
    }
}