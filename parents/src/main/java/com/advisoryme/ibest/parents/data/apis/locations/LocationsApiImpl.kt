package com.advisoryme.ibest.parents.data.apis.locations

import com.advisoryme.ibest.parents.data.apis.interceptors.AuthorizationInterceptor
import com.advisoryme.ibest.parents.utils.InjectionUtils

class LocationsApiImpl(token: String) {
    private val locationsApi: LocationsApi

    init {
        val authInterceptor = AuthorizationInterceptor(token)
        val apiManagerImpl = InjectionUtils.provideApiManager(authInterceptor)
        locationsApi = apiManagerImpl.createApi(LocationsApi::class.java)
    }

    fun locations() = locationsApi.locations()
}