package com.advisoryme.ibest.parents.data.database.daos

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.advisoryme.ibest.parents.data.models.Notification

@Dao
interface NotificationDao {

    @Query("SELECT * FROM notification ORDER BY date DESC LIMIT 50")
    fun getLatest(): List<Notification>

    @Insert
    fun insert(notification: Notification)

    @Query("DELETE FROM notification")
    fun clear()
}