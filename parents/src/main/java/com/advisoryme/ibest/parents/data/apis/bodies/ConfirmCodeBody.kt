package com.advisoryme.ibest.parents.data.apis.bodies

import com.google.gson.annotations.SerializedName

data class ConfirmCodeBody(
        @SerializedName("phone_number")
        val phoneNumber: String = "",
        @SerializedName("code")
        val code: Int = 0
)