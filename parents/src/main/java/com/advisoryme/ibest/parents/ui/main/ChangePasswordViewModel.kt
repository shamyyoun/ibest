package com.advisoryme.ibest.parents.ui.main

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import com.advisoryme.ibest.parents.R
import com.advisoryme.ibest.parents.ui.base.BaseViewModel
import com.advisoryme.ibest.parents.utils.InjectionUtils

class ChangePasswordViewModel(app: Application) : BaseViewModel(app) {
    private val activeUserRepository = InjectionUtils.provideActiveUserRepository(app)
    private val updatePasswordRepository = InjectionUtils.provideUpdatePasswordRepository(activeUserRepository.getUser()!!.token)
    val changePasswordEvent = MutableLiveData<Boolean>()

    fun changePassword(oldPassword: String, newPassword: String, reNewPassword: String) {
        // validate
        if (validateInternetConnection() && validateChangePasswordInputs(oldPassword, newPassword, reNewPassword)) {
            triggerProgress()

            updatePasswordRepository.changePassword(oldPassword = oldPassword, newPassword = newPassword).observeForever {
                triggerProgress()

                // process repo result then trigger the event
                handleRepositoryResult(it) {
                    changePasswordEvent.value = it?.result
                }
            }
        }
    }

    private fun validateChangePasswordInputs(oldPassword: String, newPassword: String, reNewPassword: String): Boolean {
        return when {
            oldPassword.isEmpty() -> {
                triggerError(R.string.enter_old_password)
                false
            }
            newPassword.isEmpty() -> {
                triggerError(R.string.enter_new_password)
                false
            }
            newPassword.length < 6 -> {
                triggerError(R.string.too_short_password)
                false
            }
            reNewPassword != newPassword -> {
                triggerError(R.string.passwords_does_not_match)
                false
            }
            else -> true
        }
    }
}