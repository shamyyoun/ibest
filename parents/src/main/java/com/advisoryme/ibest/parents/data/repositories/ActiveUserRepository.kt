package com.advisoryme.ibest.parents.data.repositories

import android.content.Context
import com.advisoryme.ibest.parents.data.models.User
import com.advisoryme.ibest.parents.utils.Const
import com.advisoryme.ibest.parents.utils.InjectionUtils

/**
 * Created by Shamyyoun on 8/27/16.
 */
class ActiveUserRepository(private val context: Context) {
    private val prefsManager = InjectionUtils.providePrefsManager(context)
    private var user: User? = null

    fun hasActiveUser() = getUser() != null

    fun getUser(): User? {
        if (user == null) {
            user = prefsManager.load(Const.KEY_ACTIVE_USER, User::class.java)
        }
        return user
    }

    fun updateUser(user: User) {
        this.user = user
        prefsManager.save(user, Const.KEY_ACTIVE_USER)
    }

    fun removeUserData() {
        if (user != null) {
            val notificationRepository = InjectionUtils.provideNotificationsRepository(context, user!!.token)
            notificationRepository.clearNotifications()
        }

        prefsManager.remove(Const.KEY_ACTIVE_USER)
        user = null
    }

    companion object {
        private var activeUserRepository: ActiveUserRepository? = null

        fun instance(context: Context): ActiveUserRepository {
            if (activeUserRepository == null) {
                activeUserRepository = ActiveUserRepository(context)
            }

            return activeUserRepository!!
        }
    }
}
